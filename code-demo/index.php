<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Decision Tree Of Weather</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
    <header>
        <div class="navbar navbar-dark bg-dark box-shadow">
            <div class="container d-flex justify-content-between">
                <a href="#" class="navbar-brand d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>
                    <strong>Nhóm 12</strong>
                </a>
            </div>
        </div>
    </header>
    <br>
    <div class="container">
        <form action="" method="GET">
            <div class="row">
                <div class="col-md-3 mb-3">
                    <label for="cc-expiration">Outlook</label>
                    <select name="outlook" class="form-control">
                        <?php   $options = array( 'sunny', 'overcast', 'rainy' );
                                $output = '';
                                for( $i=0; $i < count($options); $i++ ) {
                                    $output .= '<option value=' . $options[$i]
                                        . ( $_GET['outlook'] == $options[$i] ? ' selected="selected"' : '' ) . '>'
                                        . $options[$i]
                                        . '</option>';

                                }
                                echo $output;
                        ?>
                    </select>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="cc-expiration">Temperature</label>
                    <select name="temperature" class="form-control">
                        <?php   $options = array( 'hot', 'mild', 'cool' );
                        $output = '';
                        for( $i=0; $i < count($options); $i++ ) {
                            $output .= '<option value=' . $options[$i]
                                . ( $_GET['temperature'] == $options[$i] ? ' selected="selected"' : '' ) . '>'
                                . $options[$i]
                                . '</option>';

                        }
                        echo $output;
                        ?>
                    </select>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="cc-expiration">Humidity</label>
                    <select name="humidity" class="form-control">
                        <?php   $options = array( 'high', 'normal');
                        $output = '';
                        for( $i=0; $i < count($options); $i++ ) {
                            $output .= '<option value=' . $options[$i]
                                . ( $_GET['humidity'] == $options[$i] ? ' selected="selected"' : '' ) . '>'
                                . $options[$i]
                                . '</option>';
                        }
                        echo $output;
                        ?>
                    </select>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="cc-expiration">Windy</label>
                    <select name="windy" class="form-control">
                        <?php   $options = array( 'true', 'false'	);
                        $output = '';
                        for( $i=0; $i < count($options); $i++ ) {
                            $output .= '<option value=' . $options[$i]
                                . ( $_GET['windy'] == $options[$i] ? ' selected="selected"' : '' ) . '>'
                                . $options[$i]
                                . '</option>';
                        }
                        echo $output;
                        ?>
                    </select>
                </div>
            </div>
            <button class="btn btn-primary btn-lg btn-block" type="submit">Go to play Tennis ?</button>
        </form>
        <br>
        <?php if (isset($_GET['outlook']) && isset($_GET['temperature']) && isset($_GET['humidity']) && isset($_GET['windy'])) {?>
        <?php
             $outlook = $_GET['outlook'];
            $temperature = $_GET['temperature'];
            $humidity = $_GET['humidity'];
            $windy = $_GET['windy'];

             $result = '';
             if($outlook == 'overcast') $result = true;
             if($outlook == 'sunny')
             {
                 $result = $humidity =='high' ? false : true;
             }
             if($outlook == 'rainy')
             {
                 $result = $windy =='true' ? false : true;
             }
        ?>

            <div class="col-md-6 offset-5">
            <a class="btn btn-outline-secondary" href="#"><?php echo $result == true ? 'Go To Play Tennis' : 'Stay At Home' ?></a>
        </div>
        <?php }?>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>